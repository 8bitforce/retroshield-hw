### Contributing Guides

Thank you for your interest to contribute to RetroShield!

We've put together the following guidelines to help you figure out how/where you can best be helpful.

### Table of Contents

- [Purpose](#purpose)
- [Types of contributions we're looking for](#types-of-contributions-were-looking-for)
- [Ground rules & expectations](#ground-rules--expectations)
- [How to contribute](#how-to-contribute)
- [Setting up your environment](#setting-up-your-environment)
- [Style guide](#style-guide)
- [Community](#community)
  - [Credit](#credit)


## Purpose

Promote useful open-source hardware design with excellent collaboration and customer support.

## Types of contributions we're looking for

There are many ways you can directly contribute to the guides:

- learning about microprocessor & support chip hardware details
- develop software code to emulate hardware chips
- design hardware for microprocessor and/or support chips
- review and give feedback on hardware & software details
- hardware bring-up & validation (especially if you have the test equipment)
- software testing
- documentation

## Ground rules & expectations

Before we get started, here are a few things we expect from you (and that you should expect from others):

* Know we are all different.
* Be kind and thoughtful in your conversations around this project. We all come from different backgrounds and projects, which means we likely have different perspectives on "how open source is done." Try to listen to others rather than convince them that your way is correct.
* Read [Contributor Code of Conduct & Reporting Guidelines](./CODE_OF_CONDUCT.md). By participating in this project, you agree to abide by its terms.
* If you open a pull request, please ensure that your contribution passes all tests. If there are test failures, you will need to address them before we can merge your contribution.
* When adding content, please consider if it is widely valuable.

## How to contribute

If you'd like to contribute, start by searching through the [issues](https://github.com/github/opensource.guide/issues) and [pull requests](https://github.com/github/opensource.guide/pulls) to see whether someone else has raised a similar idea or question.

If you don't see your idea listed, and you think it fits into the goals of this guide, do one of the following:
* **If your contribution is minor,** such as a typo fix, open a pull request.
* **If your contribution is major,** such as a new guide, start by opening an issue first. That way, other people can weigh in on the discussion before you do any work.

## Setting up your environment

Currently We use the following tools:

- [Arduino IDE](https://www.arduino.cc/)
- [gEDA gschem & pcb](http://www.geda-project.org/)

## Style guide

Please pay attention to the following points:

* If work is using another work/project or was inspired by it:
  * get permission to use
  * include credits in source code or documentation.
* Hardware design files
  * Please use k65c02 project as the starting template.
  * New symbols
    * tbd
  * Design reviews are required before release to manufacturing.
* Software
  * Consistent formatting for readibility
  * Suggest tags: #FIX, #TODO, #IMPORTANT, #CREDIT


## Community

* details tbd.
* Wherever possible, do not take conversations to private channels, including contacting the maintainers directly. Keeping communication public means everybody can benefit and learn from the conversation.

### Credit

This document was adapted from [Open Source Guides](https://github.com/github/opensource.guide/blob/master/CONTRIBUTING.md)