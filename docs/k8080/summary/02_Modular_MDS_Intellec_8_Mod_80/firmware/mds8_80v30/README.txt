This archive contains the original ROM images from Bill Degan.  I got them
from Herb Johnson.  The original .HEX file was converted to mon80-o.bin. It
is in this archive and used to verify the source code creates an identical
binary image using the DOS command COMP. Bill's original work is at this link:

http://vintagecomputer.net/browse_thread.cfm?id=595 

This is a ROM image of the Intellec 8/Mod 80 Monitor, Version 3.0.

I found a 1975 Intel Manual documenting version 3.0 of this Monitor at this 
links:

http://bitsavers.informatik.uni-stuttgart.de/pdf/intel/MCS80/Intellec_8_Mod_80/Intel_Intellec_8_Mod_80_Reference_Manual_Feb75.pdf

I OCR'ed the document above and edited the result into the full source code
which is in "mon8_80v30.asm".  This source code assembles with my cross 
assembler and produced "mon8_80v30.hex", "mon8_80v30.bin", and "mon8_80v30.lst".

My tools and this and other Monitor archives are at this link:

http://www.nj7p.info/Toys/Software/Monitors/Monitors.html

Bill Beech
22 Feb 15
