# RetroShield for Arduino Mega

### Boards

Completed boards:

* `k14500b`: Retroshield for Motorola MC14500B 1-bit processor
* `k1802`: RetroShield for 1802 microprocessor
* `k2650`: RetroShield for 2650 microprocessor
* `k4004s`: RetroShield for 4004 microprocessor
* `k6120`: RetroShield for HD-6120 (PDP-8) microprocessor
* `k65c02`: RetroShield for 6502 microprocessor
* `k6803`:RetroShield for 6803 microprocessor
* `k6809e`: RetroShield for 6809E microprocessor
* `k68008`: Retroshield for Motorola 68008 microprocessor
* `k8008`: Retroshield for intel 8008 microprocessor
* `k8031`: RetroShield for 8031 microprocessor
* `k8060`: RetroShield for SC/MP (INS8060) microprocessor
* `k8085`: RetroShield for 8085 microprocessor
* `k8088`: Retroshield for intel 8088 microprocessor
* `kz80`: RetroShield for Z80 microprocessor
* `teensy`: Teensy 4.1 to Retroshield adapter board

These are in progress:

* `k8080`: RetroShield for intel 8080 microprocessor


### Tools

* DipTrace
https://diptrace.com/

* Hardware Design File Format is gEDA:
http://www.geda-project.org/

### GEDA Scripts

After installing gschem, pcb and tools, use makefile :

* `make sch`: open schematics
* `make pcb`: open pcb
* `make`: compile netlist for pcb
* `make clean`: clean up the intermediate files. 
* `make backup`: make a copy of sch + pcb in archive/ folder.
* `make gerber`: create gerber.zip for production
